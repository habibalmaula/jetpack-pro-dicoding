package id.or.fkam.jetpackmovie.ui.detail

import id.or.fkam.jetpackmovie.data.MovieEntity
import org.junit.After
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test

class DetailMovieViewModelTest{
    private var viewModel : DetailMovieViewModel? = null
    private var dummyMovie : MovieEntity? = null


    @Before
    fun setUp(){
        viewModel = DetailMovieViewModel()
        dummyMovie = MovieEntity(
            "m1",
            "Toy Story 4",
            "2019-06-19",
            "Woody has always been confident about his place in the world and that his priority is taking care of his kid, whether that's Andy or Bonnie. But when Bonnie adds a reluctant new toy called \"Forky\" to her room, a road trip adventure alongside old and new friends will show Woody how big the world can be for a toy.",
            "http://image.tmdb.org/t/p/w342/w9kR8qbmQ01HwnvK4alvnQ2ca0L.jpg",
            "http://image.tmdb.org/t/p/w342//m67smI1IIMmYzCl9axvKNULVKLr.jpg")

    }


    @After
    fun tearDown(){

    }

    @Test
    fun getMovieData(){
       // viewModel?.getMovies(dummyMovie?.movieId.toString())
        val movieEntity = viewModel?.getMovies(dummyMovie?.movieId.toString())
        assertNotNull(movieEntity)
        assertEquals(dummyMovie?.movieId,movieEntity?.movieId)
        assertEquals(dummyMovie?.movieTitle,movieEntity?.movieTitle)
        assertEquals(dummyMovie?.movieRelease,movieEntity?.movieRelease)
        assertEquals(dummyMovie?.movieDesc,movieEntity?.movieDesc)
        assertEquals(dummyMovie?.moviePoster,movieEntity?.moviePoster)
        assertEquals(dummyMovie?.movieBanner,movieEntity?.movieBanner)
    }

}