package id.or.fkam.jetpackmovie.ui.home

import org.junit.After
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test

class HomeViewModelTest{
    private var viewModel : HomeViewModel? = null


    @Before
    fun setUp(){
        viewModel = HomeViewModel()
    }

    @After
    fun tearDown(){

    }


    @Test
    fun getMovies(){
        val movieEntity = viewModel?.getMovies()
        assertNotNull(movieEntity)
        assertEquals(9, movieEntity?.size)
    }
    @Test
    fun getTv(){
        val tvEntity = viewModel?.getTv()
        assertNotNull(tvEntity)
        assertEquals(5, tvEntity?.size)

    }

}



