package id.or.fkam.jetpackmovie.ui.home

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import id.or.fkam.jetpackmovie.R
import id.or.fkam.jetpackmovie.data.TvEntity
import kotlinx.android.synthetic.main.item_tv.view.*

class TvAdapter (val context : Context, val Tv : List<TvEntity>): RecyclerView.Adapter<TvAdapter.TvViewHolder>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TvViewHolder {
        return TvViewHolder(LayoutInflater.from(context).inflate(R.layout.item_tv,parent,false))
    }

    override fun getItemCount(): Int = Tv.size

    override fun onBindViewHolder(holder: TvViewHolder, position: Int) {
        holder.bind(Tv[position])
       // val width = holder.itemView.context.resources.displayMetrics.widthPixels / 3
      //  val height = holder.itemView.context.resources.displayMetrics.widthPixels / 3
      //  holder.itemView.layoutParams.width = width - 0.toPx(holder.itemView.context)
      //  holder.itemView.layoutParams.height = height + 4.toPx(holder.itemView.context)
      //  holder.itemView.requestLayout()
    }

    class TvViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        fun bind(tvList : TvEntity){
            Glide.with(itemView.context).load(tvList.tvPoster).into(itemView.img_tv)

        }
    }

}