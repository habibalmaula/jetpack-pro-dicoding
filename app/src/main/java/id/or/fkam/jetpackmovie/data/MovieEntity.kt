package id.or.fkam.jetpackmovie.data

class MovieEntity(
    val movieId : String?,
    val movieTitle:String?,
    val movieRelease:String?,
    val movieDesc: String?,
    val moviePoster: String?,
    val movieBanner:String?
)