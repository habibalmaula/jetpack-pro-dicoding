package id.or.fkam.jetpackmovie.ui.detail

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.Glide
import id.or.fkam.jetpackmovie.R
import id.or.fkam.jetpackmovie.data.MovieEntity
import kotlinx.android.synthetic.main.activity_detail_movie.*

class DetailMovie : AppCompatActivity() {

    private lateinit var viewModel: DetailMovieViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_movie)

        viewModel = ViewModelProviders.of(this).get(DetailMovieViewModel::class.java)

        val extras = intent.extras

        if (extras!=null){
            val movieId = extras.getString("movieId")
            if (movieId!=null){
                val list = viewModel.getMovies(movieId)

                if (list != null) {
                    populateMovie(list)
                }
            }
        }

    }

    private fun populateMovie(movieEntity: MovieEntity) {

        Glide.with(applicationContext).load(movieEntity.movieBanner).into(iv_banner_detail)
        Glide.with(applicationContext).load(movieEntity.moviePoster).into(iv_poster_detail)

        tv_detail_title_movie.text = movieEntity.movieTitle
        tv_movie_desc.text = movieEntity.movieDesc

    }
}
