package id.or.fkam.jetpackmovie.ui.detail

import androidx.lifecycle.ViewModel
import id.or.fkam.jetpackmovie.data.MovieEntity
import id.or.fkam.jetpackmovie.utils.DataDummy

class DetailMovieViewModel :ViewModel(){
    private var movies : MovieEntity? = null


    fun getMovies(movieId : String) : MovieEntity?{
        for (i in 0 until DataDummy.generateDummyMovie().size){
            val movieEntity = DataDummy.generateDummyMovie()[i]
            if (movieEntity.movieId == movieId){
                movies = movieEntity
            }
        }

        return movies

    }


}