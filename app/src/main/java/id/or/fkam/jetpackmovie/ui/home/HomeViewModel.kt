package id.or.fkam.jetpackmovie.ui.home

import androidx.lifecycle.ViewModel
import id.or.fkam.jetpackmovie.data.MovieEntity
import id.or.fkam.jetpackmovie.data.TvEntity
import id.or.fkam.jetpackmovie.utils.DataDummy

class HomeViewModel: ViewModel(){
    fun getMovies() : List<MovieEntity>{
        return DataDummy.generateDummyMovie()
    }

    fun getTv() : List<TvEntity>{
        return DataDummy.generateDummyTv()
    }




}