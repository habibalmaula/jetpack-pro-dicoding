package id.or.fkam.jetpackmovie.ui.home

import android.annotation.SuppressLint
import android.graphics.Color
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.NestedScrollView
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.request.RequestOptions
import com.glide.slider.library.Animations.DescriptionAnimation
import com.glide.slider.library.SliderLayout
import com.glide.slider.library.SliderTypes.BaseSliderView
import com.glide.slider.library.SliderTypes.TextSliderView
import com.glide.slider.library.Tricks.ViewPagerEx
import kotlinx.android.synthetic.main.activity_home.*


class HomeActivity : AppCompatActivity(),BaseSliderView.OnSliderClickListener,ViewPagerEx.OnPageChangeListener {

    private lateinit var homeViewModel: HomeViewModel


    @SuppressLint("WrongConstant")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(id.or.fkam.jetpackmovie.R.layout.activity_home)

     /*   scrollContent.setOnScrollChangeListener { v: NestedScrollView?, _: Int, scrollY: Int, _: Int, _: Int ->
            val colorBackground: Int = if (scrollY < 255) {
                Color.argb(scrollY, 255, 255, 255)
            } else {
                Color.argb(255, 255, 255, 255)
            }

            mainToolbar.setBackgroundColor(colorBackground)
        }*/

        scrollContent.setOnScrollChangeListener(NestedScrollView.OnScrollChangeListener { v, scrollX, scrollY, oldScrollX, oldScrollY ->
            var color = Color.parseColor("#6200EE") // ideally a global variable
            if (scrollY < 256) {
                val alpha = scrollY shl 24 or (-1).ushr(8)
                color = color and alpha
            }
            mainToolbar.setBackgroundColor(color)
        })


        homeViewModel = ViewModelProviders.of(this).get(HomeViewModel::class.java)

        val layoutManager = LinearLayoutManager(this)
        layoutManager.isAutoMeasureEnabled

        val gridLayoutManager = GridLayoutManager(this,3,GridLayoutManager.VERTICAL, false)


        rv_tv.adapter = TvAdapter(this, homeViewModel.getTv())
        rv_tv.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)


        rv_movie.layoutManager = gridLayoutManager
        rv_movie.adapter = MovieAdapter(this, homeViewModel.getMovies())




        setBanner()

    }

    private fun setBanner() {
        //setting slider
        val requestOptions = RequestOptions()
        requestOptions.centerCrop()

        for (i in 0..3){
            val sliderView = TextSliderView(this)

            sliderView
                .image(homeViewModel.getMovies()[i].movieBanner)
                .description(homeViewModel.getMovies()[i].movieTitle)
                .setRequestOption(requestOptions)
                .setProgressBarVisible(true)
                .setOnSliderClickListener(this)

            sliderView.bundle(Bundle())
            sliderView.bundle.putString("extra",homeViewModel.getMovies()[i].movieId)
            slider_movie.addSlider(sliderView)
            slider_movie.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom)
            slider_movie.setCustomAnimation(DescriptionAnimation())
            slider_movie.setDuration(5000)
            slider_movie.addOnPageChangeListener(this)
            slider_movie.stopCyclingWhenTouch(true)



        }

    }

    override fun onStop() {
        super.onStop()
        slider_movie.stopAutoCycle()
    }


    override fun onSliderClick(slider: BaseSliderView?) {

    }

    override fun onPageScrollStateChanged(state: Int) {
    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
    }

    override fun onPageSelected(position: Int) {
    }





}
