package id.or.fkam.jetpackmovie.ui.home

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import id.or.fkam.jetpackmovie.R
import id.or.fkam.jetpackmovie.data.MovieEntity
import id.or.fkam.jetpackmovie.ui.detail.DetailMovie
import id.or.fkam.jetpackmovie.utils.toPx
import kotlinx.android.synthetic.main.item_movie.view.*

class MovieAdapter (val context: Context, val movieList: List<MovieEntity>): RecyclerView.Adapter<MovieAdapter.ViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_movie,parent,false))
    }

    override fun getItemCount(): Int = movieList.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(movieList[position])
        val width = holder.itemView.context.resources.displayMetrics.widthPixels / 3
        val height = holder.itemView.context.resources.displayMetrics.widthPixels / 3
        holder.itemView.layoutParams.width = width - 7.toPx(holder.itemView.context)
        holder.itemView.layoutParams.height = height + 80.toPx(holder.itemView.context)
        holder.itemView.requestLayout()

        holder.itemView.setOnClickListener {
            val intent = Intent(context,DetailMovie::class.java)
            intent.putExtra("movieId",movieList[position].movieId)
            Log.d("MENGAMBI ID", movieList[position].movieId)
            context.startActivity(intent)

        }
    }

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        @SuppressLint("SetTextI18n")
        fun bind(movieList: MovieEntity){
            val text = "..."
            Glide.with(itemView.context).load(movieList.moviePoster).into(itemView.iv_movie)
            itemView.tv_movie_title.text = movieList.movieTitle?.subSequence(0,7).toString()+ text
        }
    }

}