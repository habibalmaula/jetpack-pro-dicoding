package id.or.fkam.jetpackmovie.data

class TvEntity (
    val tvId: String?,
    val tvTitle: String?,
    val tvPoster: String?
)