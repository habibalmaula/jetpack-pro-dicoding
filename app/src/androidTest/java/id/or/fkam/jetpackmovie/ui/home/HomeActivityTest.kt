package id.or.fkam.jetpackmovie.ui.home

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.rule.ActivityTestRule
import id.or.fkam.jetpackmovie.R
import id.or.fkam.jetpackmovie.utils.DataDummy
import id.or.fkam.jetpackmovie.utils.RecyclerViewItemCountAssertion
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class HomeActivityTest {

    @Rule
    @JvmField
    var activityTest = ActivityTestRule(HomeActivity::class.java)


    @Before
    fun setUp() {
    }

    @After
    fun tearDown() {
    }

    @Test
    fun loadMovieandTv(){
        onView(withId(R.id.slider_movie)).check(matches(isDisplayed()))
        onView(withId(R.id.rv_tv)).check(matches(isDisplayed()))
        onView(withId(R.id.rv_movie)).check(matches(isDisplayed()))
        onView(withId(R.id.rv_tv)).check(RecyclerViewItemCountAssertion(5))
        onView(withId(R.id.rv_movie)).check(RecyclerViewItemCountAssertion(9))

    }
}