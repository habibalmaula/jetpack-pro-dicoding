package id.or.fkam.jetpackmovie.ui.detail

import android.content.Intent
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import id.or.fkam.jetpackmovie.R
import id.or.fkam.jetpackmovie.utils.DataDummy
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class DetailMovieTest{
    private val dummyMovie = DataDummy.generateDummyMovie()[0]


    @Rule
    @JvmField
    var activityTest = object : ActivityTestRule<DetailMovie>(DetailMovie::class.java){
        override fun getActivityIntent(): Intent {

            val target = InstrumentationRegistry.getInstrumentation().targetContext
            val result = Intent(target, DetailMovie::class.java)
            result.putExtra("movieId", dummyMovie.movieId)
            return result

        }
    }

    @Before
    fun setUp() {

    }

    @After
    fun tearDown() {
    }


    @Test
    fun loadDetailMovie(){
        onView(withId(R.id.iv_banner_detail)).check(matches(isDisplayed()))
        onView(withId(R.id.iv_poster_detail)).check(matches(isDisplayed()))
        onView(withId(R.id.tv_detail_title_movie)).check(matches(isDisplayed()))
        onView(withId(R.id.tv_detail_title_movie)).check(matches(withText(dummyMovie.movieTitle)))
        onView(withId(R.id.tv_movie_desc)).check(matches(isDisplayed()))
        onView(withId(R.id.tv_movie_desc)).check(matches(withText(dummyMovie.movieDesc)))

    }




}